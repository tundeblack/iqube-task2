import React, { useState } from "react";
import { hot } from "react-hot-loader";
import FormInput from "./FormInput.js";
import Button from "./Button.js";
import Anchor from "./Anchor.js";
import { CONFIRM_PAYMENT } from "../Task2App.js";


const BillingInfo = (props) => 
{
    const [ nameOnCard, setNameOnCard ] = useState("");
    const [ cardType, setCardType ] = useState("");
    const [ cardDetails, setCardDetails ] = useState("");
    const [ expiryDate, setExpiryDate ] = useState("");
    const [ cvv, setCvv] = useState("");


    return (
        <form>
            <div className = "form-group">
                <label htmlFor = "nameOnCard" className = "label">Name on Card&nbsp;<span className = "t-red">*</span></label>
                <FormInput 
                    id = "name"
                    inputType = "text"
                    name = "nameOnCard"
                    value = { nameOnCard }
                    classes = "form-control finput"
                    placeholder = "Opara Linus Ahmed"
                    onChangeCallback = {
                        (e) => 
                        {
                            setNameOnCard(e.currentTarget.value);
                        }
                    }

                />
            </div>
            <div className = "form-group">
                <label htmlFor = "cardType" className = "t-black">Card Type&nbsp;<span className = "t-red">*</span></label>
                <FormInput 
                    id = "email"
                    inputType = "comboBox"
                    name = "cardType"
                    value = { cardType }
                    comboBoxLabel = "Visa"
                    classes = "form-control finput"
                    placeholder = ""
                    onChangeCallback = {
                        (e) => 
                        {
                            setCardType(e.currentTarget.value);
                        }
                    }
                />
            </div>
            <div className = "row">
                <div className = "col-md-7">
                    <div className = "form-group">
                        <label htmlFor = "cardDetails" className = "t-black">Card details&nbsp;<span className = "t-red">*</span></label>
                        <FormInput 
                            id = "cardDetails"
                            inputType = "text"
                            name = "cardDetails"
                            value = { cardDetails }
                            classes = "form-control finput"
                            placeholder = "44960  44960  44960  44960"
                            onChangeCallback = {
                                (e) => 
                                {
                                    setCardDetails(e.currentTarget.value);
                                }
                            }
                        />
                    </div>
                </div>
                <div className = "col-md-3">
                    <div className = "form-group">
                        <label htmlFor = "expiryDate" className = "t-black">Expiry date&nbsp;<span className = "t-red">*</span></label>
                        <FormInput 
                            id = "expiryDate"
                            inputType = "text"
                            name = "expiryDate"
                            value = { expiryDate }
                            classes = "form-control finput"
                            placeholder = "04 / 23"
                            onChangeCallback = {
                                (e) => 
                                {
                                    setExpiryDate(e.currentTarget.value);
                                }
                            }
                        />
                    </div>
                </div>
                <div className = "col-md-2">
                    <div className = "form-group">
                        <label htmlFor = "cvv" className = "t-black">CVV&nbsp;<span className = "t-red">*</span></label>
                        <FormInput 
                            id = "cvv"
                            inputType = "text"
                            name = "cvv"
                            value = { cvv }
                            classes = "form-control finput"
                            placeholder = "923"
                            onChangeCallback = {
                                (e) => 
                                {
                                    setCvv(e.currentTarget.value);
                                }
                            }
                        />
                    </div>
                </div>
            </div>

            <div className = "row my-5 align-items-center">
                <div className = "col-md-5">
                    <Button 
                        classes = "btn btn-o"
                        content = "Next"
                        onClickCallback = {
                            (e) => 
                            {
                                props.next(CONFIRM_PAYMENT);
                            }
                        }
                    />
                </div>
                <div className = "col-md-5">
                    <Anchor 
                        linkTo = "#"
                        classes = "t-purple"
                        content = "Cancel Payment"
                        onClickCallback = {
                            (e) => 
                            {
                                e.preventDefault();
                                
                            }
                        }
                    />
                </div>
            </div>
        </form>
    );
}

export default hot(module)(BillingInfo);