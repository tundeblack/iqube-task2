import React from "react";
import { hot } from "react-hot-loader";
import Anchor from "./Anchor.js";
import HrLine from "./HrLine.js";
import Image from "./Image.js";
import AppConfig from "../config/AppConfig.js";
import Wrapper from "./Wrapper.js";
import { currentYear } from "../helpers/Functions.js";


const component = (props) =>
{
    return (
        <React.Fragment>
            <div className = "" style = {{ height: "150px" }}></div>
            <section className = "border-top pb-5">
                <p className = "text-center p-3 m-0">&copy; { currentYear() } Theragist Technologies Ltd</p>
                
                <Wrapper>
                    <div className = "row align-items-start pt-5">
                        <div className = "col-md-3 d-none d-md-block">
                            <Anchor 
                                type = "routerLink"
                                linkTo = "/"
                                content = {
                                    <Image 
                                        src = { AppConfig.imagesDir + "/theragist-logo.png" }
                                        style = {{ 
                                            width: "150px",
                                            height: "auto"
                                        }}
                                    />
                                }
                            />
                        </div>
                        <div className = "col-md-3">
                            <h5>Theragist</h5>
                            <ul className = "list-inline">
                                <li className = "my-1">
                                    <Anchor 
                                        content = "How It Works"
                                    />
                                </li>
                                <li className = "my-1">
                                    <Anchor 
                                        content = "Privacy Policy"
                                    />
                                </li>
                                <li className = "my-1">
                                    <Anchor 
                                        content = "Terms of Service"
                                    />
                                </li>
                                <li className = "my-1">
                                    <Anchor 
                                        content = "Contact"
                                    />
                                </li>
                            </ul>
                        </div>
                        <div className = "col-md-3">
                            <h5>Hutzpah</h5>
                            <ul className = "list-inline">
                                <li className = "my-1">
                                    <Anchor 
                                        content = "About"
                                    />
                                </li>
                                <li className = "my-1">
                                    <Anchor 
                                        content = "Team"
                                    />
                                </li>
                                <li className = "my-1">
                                    <Anchor 
                                        content = "Investor Relations"
                                    />
                                </li>
                                <li className = "my-1">
                                    <Anchor 
                                        content = "Contact"
                                    />
                                </li>
                            </ul>
                        </div>
                        <div className = "col-md-3">
                            <div className = "row">
                                <div className = "col-auto"><i className = "fab fa-facebook fa-2x"></i></div>
                                <div className = "col-auto"><i className = "fab fa-twitter fa-2x"></i></div>
                                <div className = "col-auto"><i className = "fab fa-youtube fa-2x"></i></div>
                                <div className = "col-auto"><i className = "fab fa-linkedin fa-2x"></i></div>
                            </div>
                        </div>
                    </div>
                </Wrapper>
            </section>
        </React.Fragment>
    );
}

const Footer = component;
export default hot(module)(Footer)


