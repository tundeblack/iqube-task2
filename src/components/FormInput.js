import React from "react";
import { hot } from "react-hot-loader";

function FormInput(props)
{
    let { 
        inputType, 
        classes, 
        value, 
        name, 
        placeholder, 
        required, 
        onChangeCallback,
        comboBoxLabel,
        comboBoxOptions, 
        inputDisabled,
        ...theRest 
    } = props;
    
    let render;
    
    switch (props.inputType)
    {
        case "radio":
        case "checkbox":
        case "password":
        case "file":
        case "text":
            render = <input
                        name = { name } 
                        type = { inputType }
                        className = { classes }
                        value = { value }
                        onChange = { onChangeCallback }
                        placeholder = { placeholder }
                        required = { required }
                        disabled = { inputDisabled }
                        { ...theRest }
                    />;
            break;
        case "comboBox":
            let optionsRender = [<option key={0} value = "">{ comboBoxLabel }</option>];
            if ((typeof comboBoxOptions !== undefined) && Array.isArray(comboBoxOptions))
            {
                for (let i = 0; i < comboBoxOptions.length; i++)
                {
                    optionsRender.push(
                        <option 
                            key = { (i + 1) } 
                            value = { comboBoxOptions[i].value }
                        >
                            { comboBoxOptions[i].text }
                        </option>
                    );
                }
            }
            render = <select
                        name = { name } 
                        value = { value }
                        onChange = { onChangeCallback }
                        required = { required }
                        className = { classes }
                        disabled = { inputDisabled }

                        { ...theRest }
                    >
                        { optionsRender }
                    </select>
            break;
        default:
            render = null;
    }

    return render;
}

export default hot(module)(FormInput);