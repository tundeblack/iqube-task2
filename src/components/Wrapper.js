import React from "react";
import { hot } from "react-hot-loader";
import Header from "./Header.js";

const Wrapper = (props) => 
{
    return (
        <section>
            <div className="container">
                <div className = "row justify-content-center">
                    <div className = "col-md-7">
                        <Header 
                            activeStep = { props.activeStep }
                        />
                    </div>
                </div>
                <div className = "row justify-content-center">
                    <div className = "col-md-7">
                        {
                            props.children
                        }
                    </div>
                </div>        
            </div>
        </section>
    );
}

export default hot(module)(Wrapper);
