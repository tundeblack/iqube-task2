import React, { useState } from "react";
import { hot } from "react-hot-loader";
import FormInput from "./FormInput.js";
import Button from "./Button.js";
import Anchor from "./Anchor.js";
import { BILLING_INFO } from "../Task2App.js";


const PersonalInfo = (props) => 
{
    const [ name, setName ] = useState("");
    const [ email, setEmail ] = useState("");
    const [ address1, setAddress1 ] = useState("");
    const [ address2, setAddress2 ] = useState("");
    const [ localGovt, setLocalGovt ] = useState("");
    const [ state, setState ] = useState("");


    return (
        <form>
            <div className = "form-group">
                <label htmlFor = "name" className = "label">Name</label>
                <FormInput 
                    id = "name"
                    inputType = "text"
                    name = "name"
                    value = { name }
                    classes = "form-control finput"
                    placeholder = "Opara Linus Ahmed"
                    onChangeCallback = {
                        (e) => 
                        {
                            setName(e.currentTarget.value);
                        }
                    }
                />
            </div>
            <div className = "form-group">
                <label htmlFor = "email" className = "label">
                    Email Address&nbsp;<span className = "t-red">*</span><br />
                    <span className = "label-small">The purchase receipt would be sent to this email address.</span>
                </label>
                <FormInput 
                    id = "email"
                    inputType = "text"
                    name = "email"
                    value = { email }
                    classes = "form-control finput"
                    placeholder = "OparaLinusAhmed@devmail.com"
                    onChangeCallback = {
                        (e) => 
                        {
                            setEmail(e.currentTarget.value);
                        }
                    }
                />
            </div>
            <div className = "form-group">
                <label htmlFor = "address1" className = "label">Address 1</label>
                <FormInput 
                    id = "address1"
                    inputType = "text"
                    name = "address1"
                    value = { address1 }
                    classes = "form-control finput"
                    placeholder = "The address of the user goes here"
                    onChangeCallback = {
                        (e) => 
                        {
                            setAddress1(e.currentTarget.value);
                        }
                    }
                />
            </div>
            <div className = "form-group">
                <label htmlFor = "address2" className = "label">Address 2</label>
                <FormInput 
                    id = "name"
                    inputType = "text"
                    name = "address2"
                    value = { address2 }
                    classes = "form-control finput"
                    placeholder = "and here"
                    onChangeCallback = {
                        (e) => 
                        {
                            setAddress2(e.currentTarget.value);
                        }
                    }
                />
            </div>
            <div className = "row">
                <div className = "col-md-8">
                    <div className = "form-group">
                        <label htmlFor = "localGovt" className = "label">Local Government</label>
                        <FormInput 
                            id = "localGovt"
                            inputType = "text"
                            name = "localGovt"
                            value = { localGovt }
                            classes = "form-control finput"
                            placeholder = "Surulere"
                            onChangeCallback = {
                                (e) => 
                                {
                                    setLocalGovt(e.currentTarget.value);
                                }
                            }
                        />
                    </div>
                </div>
                <div className = "col-md-4">
                    <div className = "form-group">
                        <label htmlFor = "state" className = "label">State</label>
                        <FormInput 
                            id = "state"
                            inputType = "comboBox"
                            name = "state"
                            value = { state }
                            classes = "form-control finput"
                            comboBoxLabel = "Lagos"
                            onChangeCallback = {
                                (e) => 
                                {
                                    setState(e.currentTarget.value);
                                }
                            }
                        />
                    </div>
                </div>
            </div>

            <div className = "row my-5 align-items-center">
                <div className = "col-md-5">
                    <Button 
                        classes = "btn btn-o"
                        content = "Next"
                        onClickCallback = {
                            (e) => 
                            {
                                props.next(BILLING_INFO);
                            }
                        }
                    />
                </div>
                <div className = "col-md-5">
                    <Anchor 
                        linkTo = "#"
                        classes = "t-purple"
                        content = "Cancel Payment"
                        onClickCallback = {
                            (e) => 
                            {
                                e.preventDefault();
                                
                            }
                        }
                    />
                </div>
            </div>
        </form>
    );
}

export default hot(module)(PersonalInfo);