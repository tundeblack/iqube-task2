import React, { useState } from "react";
import { hot } from "react-hot-loader";
import { PURCHASE_COMPLETED } from "../Task2App.js";
import Anchor from "../components/Anchor.js";
import Button from "../components/Button.js";


const ConfirmPaymentForm = (props) => 
{
    return (
        <section>
            <div className = "container-fluid confirm-payment-box">
                <div className = "row confirm-payment-header-blue-background p-3">
                    <div className = "col-md-9 t-white">Item Name</div>
                    <div className = "col-md-3 t-white">&#8358;&nbsp;Price</div>
                </div>
                <div className = "row px-4 pt-5">
                    <div className = "col-md-9">
                        <p className = "t-purple-n">Data science and usability</p>
                        <p className = "t-purple-n">Shipping</p>
                    </div>
                    <div className = "col-md-3">
                        <p className = "text-right t-purple-n">50,000.00</p>
                        <p className = "text-right">0.00</p>
                    </div>
                </div>
                
                <div className = "row">
                    <div className = "col-12"><hr /></div>
                </div>

                <div className = "row pt-3 pb-5">
                    <div className = "col-12 px-5">
                        <div className = "row confirm-payment-total-box p-1">
                            <div className = "col-md-8">Total</div>
                            <div className = "col-md-4 text-right t-purple-n">50,000.00</div>
                        </div>                    
                    </div>
                </div>
            </div>

            <div className = "row my-5 align-items-center">
                <div className = "col-md-5">
                    <Button 
                        classes = "btn btn-o"
                        content = "Next"
                        onClickCallback = {
                            (e) => 
                            {
                                props.next(PURCHASE_COMPLETED);
                            }
                        }
                    />
                </div>
                <div className = "col-md-5">
                    <Anchor 
                        linkTo = "#"
                        classes = "t-purple"
                        content = "Cancel Payment"
                        onClickCallback = {
                            (e) => 
                            {
                                e.preventDefault();    
                            }
                        }
                    />
                </div>
            </div>
        </section>
    );
}

export default hot(module)(ConfirmPaymentForm);