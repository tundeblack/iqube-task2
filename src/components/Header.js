import React, { useState, useEffect } from "react";
import { hot } from "react-hot-loader";
import {  
    PERSONAL_INFO,
    BILLING_INFO,
    CONFIRM_PAYMENT
} from "../Task2App.js";


const Header = (props) =>
{
    return (
        <section className = "my-4">
            <h3 className = "heading mb-5">Complete your Purchase</h3>
            <div className = "row">
                <div className = "col-4">
                    <h5 className = { 
                        (props.activeStep == PERSONAL_INFO) ? "sub-heading-o" : "sub-heading-g" 
                    }>Personal Info</h5>
                </div>
                <div className = "col-4">
                    <h5 className = { 
                        (props.activeStep == BILLING_INFO) ? "sub-heading-o" : "sub-heading-g" 
                    }>Billing Info</h5>
                </div>
                <div className = "col-4">
                    <h5 className = { 
                        (props.activeStep == CONFIRM_PAYMENT) ? "sub-heading-o" : "sub-heading-g" 
                    }>Confirm Payment</h5>
                </div>
            </div>    
            <div className = "row align-items-center">
                <div className = "col-4 p-0">
                    <hr className = { (props.activeStep == PERSONAL_INFO) ? "line-o" : "line-b" } />
                </div>
                <div className = "col-4 p-0">
                    <hr className = { (props.activeStep == BILLING_INFO) ? "line-o" : "line-b" } />
                </div>
                <div className = "col-4 p-0">
                    <hr className = { (props.activeStep == CONFIRM_PAYMENT) ? "line-o" : "line-b" } />
                </div>
            </div>    
        </section>
    );
}

export default hot(module)(Header);