import React, { useState, useEffect } from "react";
import { hot } from "react-hot-loader";
import PersonalInfo from "./views/PersonalInfo.js";
import BillingInfo from "./views/BillingInfo.js";
import ConfirmPayment from "./views/ConfirmPayment.js";
import PurchaseCompleted from "./views/PurchaseCompleted.js";


export const PERSONAL_INFO = 0, 
             BILLING_INFO = 1,
             CONFIRM_PAYMENT = 2,
             PURCHASE_COMPLETED = 3;

const Task2App = (props) => 
{
    const [ currView, setCurrView ] = useState(PERSONAL_INFO);

    let component = <PersonalInfo 
        setNext = { setCurrView }
    />;

    switch (currView)
    {
        case BILLING_INFO:
            component = <BillingInfo 
                setNext = { setCurrView }
            />;
            break;
        case CONFIRM_PAYMENT:
            component = <ConfirmPayment  
                setNext = { setCurrView }
            />;
            break;
        case PURCHASE_COMPLETED:
            component = <PurchaseCompleted 
                setNext = { setCurrView }
            />;
            break;
    }

    return (
        <section>
            { component }
        </section>
    );
}

export default hot(module)(Task2App);