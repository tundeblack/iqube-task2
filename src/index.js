import React from "react";
import ReactDom from "react-dom";
import Task2App from "./Task2App.js";

ReactDom.render(
    <Task2App />,
    document.getElementById("appRoot")
);