import React from "react";
import { hot } from "react-hot-loader";
import BillingInfoForm from "../components/BillingInfoForm.js";
import Wrapper from "../components/Wrapper.js";
import { BILLING_INFO } from "../Task2App.js";


const BillingInfo = (props) => 
{
    return (
        <section>
            <Wrapper activeStep = { BILLING_INFO } >
                <BillingInfoForm 
                    next = { props.setNext }
                />
            </Wrapper>
        </section>
    );
}

export default hot(module)(BillingInfo);