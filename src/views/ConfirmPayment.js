import React from "react";
import { hot } from "react-hot-loader";
import ConfirmPaymentForm from "../components/ConfirmPaymentForm.js";
import Wrapper from "../components/Wrapper.js";
import { CONFIRM_PAYMENT } from "../Task2App.js";


const ConfirmPayment = (props) => 
{
    return (
        <section>
            <Wrapper activeStep = { CONFIRM_PAYMENT } >
                <ConfirmPaymentForm 
                    next = { props.setNext }
                />
            </Wrapper>
        </section>
    );
}

export default hot(module)(ConfirmPayment);