import React from "react";
import { hot } from "react-hot-loader";
import PersonalInfoForm from "../components/PersonalInfoForm.js";
import Wrapper from "../components/Wrapper.js";
import { PERSONAL_INFO } from "../Task2App.js";


const PersonalInfo = (props) => 
{
    return (
        <section>
            <Wrapper activeStep = { PERSONAL_INFO } >
                <PersonalInfoForm
                    next = { props.setNext }
                />
            </Wrapper>
        </section>
    );
}

export default hot(module)(PersonalInfo);