import React from "react";
import { hot } from "react-hot-loader";
import Anchor from "../components/Anchor.js";
import { PERSONAL_INFO } from "../Task2App.js";


const PurchaseCompleted = (props) => 
{
    return (
        <section className = "container my-5">
            <div className = "row justify-content-center">
                <div className = "col-md-7 purchase-completed-box p-5">
                
                    <div className = "p-5">
                        <div className = "text-center my-2">
                            <span className = "fa-stack fa-2x">
                                <i className = "fa fa-circle fa-stack-2x i-circle"></i>
                                <i className = "fa fa-check fa-stack-1x green"></i>
                            </span>
                        </div>
                        
                        <div>
                            <h2 className = "heading">Purchase Completed</h2>
                        </div>

                        <div className = "my-4">
                            <p>Please check your email for details concerning this transaction.</p>
                        </div>
                        
                        <div>
                            <a
                                href = "#"
                                className = "t-orange"
                                onClick = {
                                    (e) => 
                                    {
                                        e.preventDefault();
                                        props.setNext(PERSONAL_INFO);
                                    }
                                }
                            >Return Home</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    );
}

export default hot(module)(PurchaseCompleted);